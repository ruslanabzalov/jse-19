package tsc.abzalov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.IService;
import tsc.abzalov.tm.enumeration.Role;
import tsc.abzalov.tm.model.User;

public interface IUserService extends IService<User> {

    void create(
            @NotNull String login, @NotNull String password,
            @NotNull Role role, @NotNull String firstName,
            @Nullable String lastName, @NotNull String email
    ) throws Exception;

    void create(
            @NotNull String login, @NotNull String password,
            @NotNull String firstName, @Nullable String lastName,
            @NotNull String email
    ) throws Exception;

    @Nullable
    User findByLogin(@NotNull String login) throws Exception;

    @Nullable
    User findByEmail(@NotNull String email) throws Exception;

    @Nullable
    User editPassword(@NotNull String userId, @NotNull String newPassword) throws Exception;

    @Nullable
    User editUserInfo(@NotNull String userId, @NotNull String firstName, @Nullable String lastName) throws Exception;

}
