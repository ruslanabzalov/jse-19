package tsc.abzalov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.model.Project;
import tsc.abzalov.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    int indexOf(@NotNull String userId, @NotNull Task task) throws Exception;

    boolean hasData(@NotNull String userId) throws Exception;

    void addTaskToProjectById(
            @NotNull String userId, @NotNull String projectId, @NotNull String taskId
    ) throws Exception;

    @Nullable
    Project findProjectById(@NotNull String userId, @NotNull String id) throws Exception;

    @Nullable
    Task findTaskById(@NotNull String userId, @NotNull String id) throws Exception;

    @NotNull
    List<Task> findProjectTasksById(@NotNull String userId, @NotNull String projectId) throws Exception;

    void deleteProjectById(@NotNull String userId, @NotNull String id) throws Exception;

    void deleteProjectTasksById(@NotNull String userId, @NotNull String projectId) throws Exception;

    void deleteProjectTaskById(@NotNull String userId, @NotNull String projectId) throws Exception;

}
