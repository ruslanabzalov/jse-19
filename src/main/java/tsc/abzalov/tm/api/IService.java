package tsc.abzalov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IService<T> {

    long size();

    boolean isEmpty();

    void create(@NotNull T entity);

    @NotNull
    List<T> findAll();

    @Nullable
    T findById(@NotNull String id) throws Exception;

    void clear();

    void removeById(@NotNull String id) throws Exception;

}
