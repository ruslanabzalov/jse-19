package tsc.abzalov.tm.enumeration;

public enum Status {

    TODO("TODO"),
    IN_PROGRESS("In Progress"),
    DONE("Done");

    private final String displayName;

    Status(final String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

}
