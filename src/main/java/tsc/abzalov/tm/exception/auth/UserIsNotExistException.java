package tsc.abzalov.tm.exception.auth;

import org.jetbrains.annotations.NotNull;
import tsc.abzalov.tm.exception.AbstractException;

public final class UserIsNotExistException extends AbstractException {

    public UserIsNotExistException(@NotNull String id) {
        super("User with id '" + id + "' is not exist. Please, try login with another id.");
    }

}
