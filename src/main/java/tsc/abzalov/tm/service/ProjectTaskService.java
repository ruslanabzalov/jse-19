package tsc.abzalov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.repository.IProjectRepository;
import tsc.abzalov.tm.api.repository.ITaskRepository;
import tsc.abzalov.tm.api.service.IProjectTaskService;
import tsc.abzalov.tm.exception.data.EmptyEntityException;
import tsc.abzalov.tm.exception.data.EmptyIdException;
import tsc.abzalov.tm.model.Project;
import tsc.abzalov.tm.model.Task;

import java.util.List;

import static org.apache.commons.lang3.StringUtils.isAnyBlank;
import static org.apache.commons.lang3.StringUtils.isBlank;

public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IProjectRepository projectRepository;

    @NotNull
    private final ITaskRepository taskRepository;

    public ProjectTaskService(@NotNull final IProjectRepository projectRepository,
                              @NotNull final ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public int indexOf(@NotNull final String userId, @NotNull final Task task) throws Exception {
        if (isBlank(userId)) throw new EmptyIdException();
        if (task == null) throw new EmptyEntityException();
        return taskRepository.indexOf(userId, task);
    }

    @Override
    public boolean hasData(@NotNull final String userId) throws Exception {
        if (isBlank(userId)) throw new EmptyIdException();
        return projectRepository.size(userId) != 0 && taskRepository.size(userId) != 0;
    }

    @Override
    public void addTaskToProjectById(
            @NotNull final String userId, @NotNull final String projectId, @NotNull final String taskId
    ) throws Exception {
        if (isAnyBlank(userId, projectId, taskId)) throw new EmptyIdException();
        taskRepository.addTaskToProjectById(userId, projectId, taskId);
    }

    @Override
    @Nullable
    public Project findProjectById(@NotNull final String userId, @NotNull final String id) throws Exception {
        if (isAnyBlank(userId, id)) throw new EmptyIdException();
        return projectRepository.findById(userId, id);
    }

    @Override
    @Nullable
    public Task findTaskById(@NotNull final String userId, @NotNull final String id) throws Exception {
        if (isAnyBlank(userId, id)) throw new EmptyIdException();
        return taskRepository.findById(userId, id);
    }

    @Override
    @NotNull
    public List<Task> findProjectTasksById(
            @NotNull final String userId, @NotNull final String projectId
    ) throws Exception {
        if (isAnyBlank(userId, projectId)) throw new EmptyIdException();
        return taskRepository.findProjectTasksById(userId, projectId);
    }

    @Override
    public void deleteProjectById(@NotNull final String userId, @NotNull final String id) throws Exception {
        if (isAnyBlank(userId, id)) throw new EmptyIdException();
        projectRepository.removeById(userId, id);
    }

    @Override
    public void deleteProjectTasksById(
            @NotNull final String userId, @NotNull final String projectId
    ) throws Exception {
        if (isAnyBlank(userId, projectId)) throw new EmptyIdException();
        taskRepository.deleteProjectTasksById(userId, projectId);
    }

    @Override
    public void deleteProjectTaskById(@NotNull final String userId, @NotNull final String projectId) throws Exception {
        if (isAnyBlank(userId, projectId)) throw new EmptyIdException();
        taskRepository.deleteProjectTaskById(userId, projectId);
    }

}
