package tsc.abzalov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.repository.IUserRepository;
import tsc.abzalov.tm.api.service.IUserService;
import tsc.abzalov.tm.enumeration.Role;
import tsc.abzalov.tm.exception.auth.EmptyEmailException;
import tsc.abzalov.tm.exception.auth.EmptyFirstNameException;
import tsc.abzalov.tm.exception.auth.IncorrectCredentialsException;
import tsc.abzalov.tm.exception.data.EmptyIdException;
import tsc.abzalov.tm.model.User;

import static org.apache.commons.lang3.StringUtils.isAnyBlank;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static tsc.abzalov.tm.util.HashUtil.hash;

public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final IUserRepository userRepository;

    public UserService(@NotNull final IUserRepository userRepository) {
        super(userRepository);
        this.userRepository = userRepository;
    }

    @Override
    public void create(
            @NotNull final String login, @NotNull final String password,
            @NotNull final Role role, @NotNull final String firstName,
            @Nullable final String lastName, @NotNull final String email
    ) throws Exception {
        if (isAnyBlank(login, password)) throw new IncorrectCredentialsException();
        if (isBlank(firstName)) throw new EmptyFirstNameException();
        if (isBlank(email)) throw new EmptyEmailException();
        this.create(new User(login, hash(password), role, firstName, lastName, email));
    }

    @Override
    public void create(
            @NotNull final String login, @NotNull final String password,
            @NotNull final String firstName, @Nullable final String lastName,
            @NotNull final String email
    ) throws Exception {
        if (isAnyBlank(login, password)) throw new IncorrectCredentialsException();
        if (isBlank(firstName)) throw new EmptyFirstNameException();
        if (isBlank(email)) throw new EmptyEmailException();
        this.create(new User(login, hash(password), firstName, lastName, email));
    }

    @Override
    public User findByLogin(@NotNull final String login) throws Exception {
        if (isBlank(login)) throw new IncorrectCredentialsException();
        return userRepository.findByLogin(login);
    }

    @Override
    public User findByEmail(@NotNull final String email) throws Exception {
        if (isBlank(email)) throw new EmptyEmailException();
        return userRepository.findByEmail(email);
    }

    @Override
    public User editPassword(@NotNull final String userId, @NotNull final String newPassword) throws Exception {
        if (isBlank(userId)) throw new EmptyIdException();
        if (isBlank(newPassword)) throw new IncorrectCredentialsException();
        return userRepository.editPassword(userId, hash(newPassword));
    }

    @Override
    @Nullable
    public User editUserInfo(
            @NotNull final String userId, @NotNull final String firstName, @Nullable final String lastName
    ) throws Exception {
        if (isBlank(userId)) throw new EmptyIdException();
        if (isBlank(firstName)) throw new EmptyFirstNameException();
        return userRepository.editUserInfo(userId, firstName, lastName);
    }

}
