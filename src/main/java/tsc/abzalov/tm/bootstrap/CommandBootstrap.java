package tsc.abzalov.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import tsc.abzalov.tm.api.repository.ICommandRepository;
import tsc.abzalov.tm.api.repository.IProjectRepository;
import tsc.abzalov.tm.api.repository.ITaskRepository;
import tsc.abzalov.tm.api.repository.IUserRepository;
import tsc.abzalov.tm.api.service.IProjectTaskService;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.api.service.IAuthService;
import tsc.abzalov.tm.api.service.ICommandService;
import tsc.abzalov.tm.api.service.ILoggerService;
import tsc.abzalov.tm.api.service.IProjectService;
import tsc.abzalov.tm.api.service.ITaskService;
import tsc.abzalov.tm.api.service.IUserService;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.command.auth.AuthChangePasswordCommand;
import tsc.abzalov.tm.command.auth.AuthLoginCommand;
import tsc.abzalov.tm.command.auth.AuthLogoffCommand;
import tsc.abzalov.tm.command.auth.AuthRegisterCommand;
import tsc.abzalov.tm.command.project.*;
import tsc.abzalov.tm.command.interaction.CommandAddTaskToProject;
import tsc.abzalov.tm.command.interaction.CommandDeleteProjectTask;
import tsc.abzalov.tm.command.interaction.CommandDeleteProjectTasks;
import tsc.abzalov.tm.command.interaction.CommandShowProjectTasks;
import tsc.abzalov.tm.command.sorting.*;
import tsc.abzalov.tm.command.system.*;
import tsc.abzalov.tm.command.task.*;
import tsc.abzalov.tm.command.user.UserChangeInfoCommand;
import tsc.abzalov.tm.command.user.UserShowInfoCommand;
import tsc.abzalov.tm.repository.CommandRepository;
import tsc.abzalov.tm.repository.ProjectRepository;
import tsc.abzalov.tm.repository.TaskRepository;
import tsc.abzalov.tm.repository.UserRepository;
import tsc.abzalov.tm.service.AuthService;
import tsc.abzalov.tm.service.CommandService;
import tsc.abzalov.tm.service.ProjectTaskService;
import tsc.abzalov.tm.service.LoggerService;
import tsc.abzalov.tm.service.ProjectService;
import tsc.abzalov.tm.service.TaskService;
import tsc.abzalov.tm.service.UserService;

import java.util.Arrays;
import java.util.List;

import static org.apache.commons.lang3.ArrayUtils.isEmpty;
import static org.apache.commons.lang3.StringUtils.isEmpty;
import static tsc.abzalov.tm.enumeration.Role.ADMIN;
import static tsc.abzalov.tm.util.InputUtil.INPUT;

public final class CommandBootstrap implements IServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final ProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    private final IUserService userService = new UserService(userRepository);

    @NotNull
    private final IAuthService authService = new AuthService(userService);

    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    public CommandBootstrap() {
        initCommand(new SystemAboutCommand());
        initCommand(new SystemArgumentsCommand());
        initCommand(new SystemCommandsCommand());
        initCommand(new SystemExitCommand());
        initCommand(new SystemHelpCommand());
        initCommand(new SystemInfoCommand());
        initCommand(new SystemVersionCommand());
        initCommand(new ProjectCreateCommand());
        initCommand(new ProjectDeleteAllCommand());
        initCommand(new ProjectDeleteByIdCommand());
        initCommand(new ProjectDeleteByIndexCommand());
        initCommand(new ProjectDeleteByNameCommand());
        initCommand(new ProjectEndByIdCommand());
        initCommand(new ProjectShowAllCommand());
        initCommand(new ProjectShowByIdCommand());
        initCommand(new ProjectShowByIndexCommand());
        initCommand(new ProjectShowByNameCommand());
        initCommand(new ProjectStartByIdCommand());
        initCommand(new ProjectUpdateByIdCommand());
        initCommand(new ProjectUpdateByIndexCommand());
        initCommand(new ProjectUpdateByNameCommand());
        initCommand(new TaskCreateCommand());
        initCommand(new TaskDeleteAllCommand());
        initCommand(new TaskDeleteByIdCommand());
        initCommand(new TaskDeleteByIndexCommand());
        initCommand(new TaskDeleteByNameCommand());
        initCommand(new TaskEndByIdCommand());
        initCommand(new TaskShowAllCommand());
        initCommand(new TaskShowByIdCommand());
        initCommand(new TaskShowByIndexCommand());
        initCommand(new TaskShowByNameCommand());
        initCommand(new TaskStartByIdCommand());
        initCommand(new TaskUpdateByIdCommand());
        initCommand(new TaskUpdateByIndexCommand());
        initCommand(new TaskUpdateByNameCommand());
        initCommand(new CommandAddTaskToProject());
        initCommand(new CommandDeleteProjectTask());
        initCommand(new CommandDeleteProjectTasks());
        initCommand(new CommandShowProjectTasks());
        initCommand(new SortingProjectsByEndDateCommand());
        initCommand(new SortingProjectsByNameCommand());
        initCommand(new SortingProjectsByStartDateCommand());
        initCommand(new SortingProjectsByStatusCommand());
        initCommand(new SortingTasksByEndDateCommand());
        initCommand(new SortingTasksByNameCommand());
        initCommand(new SortingTasksByStartDateCommand());
        initCommand(new SortingTasksByStatusCommand());
        initCommand(new AuthRegisterCommand());
        initCommand(new AuthLoginCommand());
        initCommand(new AuthLogoffCommand());
        initCommand(new AuthChangePasswordCommand());
        initCommand(new UserShowInfoCommand());
        initCommand(new UserChangeInfoCommand());
    }

    {
        try {
            userService.create("admin", "admin", ADMIN, "Admin", "", "admin@mail.com");
            userService.create("test", "test", "Test", "", "test@mail.com");
        } catch (Exception exception) {
            loggerService.error(exception);
        }
    }

    @Override
    @NotNull
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    @NotNull
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    @NotNull
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    @NotNull
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    @NotNull
    public IAuthService getAuthService() {
        return authService;
    }

    @Override
    @NotNull
    public IUserService getUserService() {
        return userService;
    }

    public void run(@NotNull final String... args) {
        System.out.println();
        try {
            if (areArgExists(args)) return;
        } catch (Exception exception) {
            loggerService.error(exception);
            return;
        }

        loggerService.info("****** WELCOME TO TASK MANAGER APPLICATION ******");
        System.out.println("Please, use \"help\" command to see all available commands.\n");
        System.out.println("Please, login/register new user to start work with the application.\n");

        @NotNull final List<String> availableStartupCommands =
                Arrays.asList("register", "login", "logoff", "help", "exit");
        @NotNull String commandName;
        //noinspection InfiniteLoopStatement
        while (true) {
            System.out.print("Please, enter your command: ");
            commandName = INPUT.nextLine();
            System.out.println();
            if (isEmpty(commandName)) continue;
            if (authService.isSessionInactive()) {
                if (!availableStartupCommands.contains(commandName)) {
                    System.out.println("Session is inactive! Please, register new user or login.");
                    continue;
                }
            }

            try {
                loggerService.command(commandName);
                commandService.getCommandByName(commandName).execute();
            } catch (Exception exception) {
                loggerService.error(exception);
            }
        }
    }

    private void initCommand(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private boolean areArgExists(@NotNull final String... args) throws Exception {
        if (isEmpty(args)) return false;
        @NotNull final String arg = args[0];
        if (isEmpty(arg)) return false;
        commandService.getArgumentByName(arg).execute();
        return true;
    }

}
