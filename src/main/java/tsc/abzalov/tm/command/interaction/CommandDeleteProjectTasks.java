package tsc.abzalov.tm.command.interaction;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IProjectTaskService;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;
import tsc.abzalov.tm.exception.general.CommandInitException;
import tsc.abzalov.tm.util.InputUtil;

import static tsc.abzalov.tm.enumeration.CommandType.INTERACTION_COMMAND;

public final class CommandDeleteProjectTasks extends AbstractCommand {

    @Override
    @NotNull
    public String getCommandName() {
        return "delete-project-with-tasks";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Delete project with tasks.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return INTERACTION_COMMAND;
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null) throw new CommandInitException();
        System.out.println("DELETE PROJECTS WITH TASKS\n");
        @NotNull final IProjectTaskService projectTasksService = serviceLocator.getProjectTaskService();
        @NotNull final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();
        if (projectTasksService.hasData(currentUserId)) {
            System.out.println("Project");
            @NotNull final String projectId = InputUtil.inputId();
            System.out.println();
            if (projectTasksService.findProjectById(currentUserId, projectId) == null) {
                System.out.println("Project was not found.\n");
                return;
            }
            projectTasksService.deleteProjectTasksById(currentUserId, projectId);
            projectTasksService.deleteProjectById(currentUserId, projectId);
            System.out.println("Project and it tasks was successfully deleted.\n");
            return;
        }
        System.out.println("One of the lists is empty!");
    }

}
