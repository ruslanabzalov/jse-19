package tsc.abzalov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IProjectService;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;
import tsc.abzalov.tm.exception.general.CommandInitException;
import tsc.abzalov.tm.model.Project;

import static tsc.abzalov.tm.enumeration.CommandType.PROJECT_COMMAND;
import static tsc.abzalov.tm.util.InputUtil.inputId;

public final class ProjectStartByIdCommand extends AbstractCommand {

    @Override
    @NotNull
    public String getCommandName() {
        return "start-project-by-id";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Start project by id.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return PROJECT_COMMAND;
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null) throw new CommandInitException();
        System.out.println("START PROJECT BY ID\n");
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        @NotNull final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();
        final boolean areProjectsExist = projectService.size(currentUserId) != 0;
        if (areProjectsExist) {
            @Nullable final Project project = projectService.startById(currentUserId, inputId());
            if (project == null) {
                System.out.println("Project was not started! Please, check that project exists or it has correct status.\n");
                return;
            }
            System.out.println("Project was successfully started.\n");
            return;
        }
        System.out.println("Projects list is empty.\n");
    }

}
