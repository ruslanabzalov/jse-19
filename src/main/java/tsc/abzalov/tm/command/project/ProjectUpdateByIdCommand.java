package tsc.abzalov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IProjectService;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;
import tsc.abzalov.tm.exception.general.CommandInitException;
import tsc.abzalov.tm.model.Project;

import static tsc.abzalov.tm.enumeration.CommandType.PROJECT_COMMAND;
import static tsc.abzalov.tm.util.InputUtil.*;

public final class ProjectUpdateByIdCommand extends AbstractCommand {

    @Override
    @NotNull
    public String getCommandName() {
        return "update-project-by-id";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Update project by id.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return PROJECT_COMMAND;
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null) throw new CommandInitException();
        System.out.println("EDIT PROJECT BY ID\n");
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        @NotNull final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();
        final boolean areProjectsExist = projectService.size(currentUserId) != 0;
        if (areProjectsExist) {
            @NotNull final String projectId = inputId();
            @NotNull final String projectName = inputName();
            @NotNull final String projectDescription = inputDescription();
            System.out.println();
            @Nullable final Project project =
                    projectService.editById(currentUserId, projectId, projectName, projectDescription);
            if (project == null) {
                System.out.println("Project was not updated! Please, check that project exists and try again.");
                return;
            }
            System.out.println("Project was successfully updated.");
            return;
        }
        System.out.println("Projects list is empty.\n");
    }

}
