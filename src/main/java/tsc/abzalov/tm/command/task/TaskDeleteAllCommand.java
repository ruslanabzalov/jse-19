package tsc.abzalov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.ITaskService;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;
import tsc.abzalov.tm.exception.general.CommandInitException;

import static tsc.abzalov.tm.enumeration.CommandType.TASK_COMMAND;

public final class TaskDeleteAllCommand extends AbstractCommand {

    @Override
    @NotNull
    public String getCommandName() {
        return "delete-all-tasks";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Delete all tasks.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return TASK_COMMAND;
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null) throw new CommandInitException();
        System.out.println("TASKS DELETION\n");
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        @NotNull final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();
        final boolean areTasksExist = taskService.size(currentUserId) != 0;
        if (areTasksExist) {
            taskService.clear(currentUserId);
            System.out.println("Tasks were deleted.\n");
            return;
        }
        System.out.println("Tasks list is empty.\n");
    }

}
