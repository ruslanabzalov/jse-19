package tsc.abzalov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;
import tsc.abzalov.tm.exception.general.CommandInitException;
import tsc.abzalov.tm.model.Task;

import static tsc.abzalov.tm.enumeration.CommandType.TASK_COMMAND;
import static tsc.abzalov.tm.util.InputUtil.inputDescription;
import static tsc.abzalov.tm.util.InputUtil.inputName;

public final class TaskCreateCommand extends AbstractCommand {

    @Override
    @NotNull
    public String getCommandName() {
        return "create-task";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Create task.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return TASK_COMMAND;
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null) throw new CommandInitException();
        System.out.println("TASK CREATION\n");
        @NotNull final String taskName = inputName();
        @NotNull final String taskDescription = inputDescription();
        @NotNull final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();
        serviceLocator.getTaskService().create(new Task(taskName, taskDescription, currentUserId));
        System.out.println("Task was successfully created.\n");
    }

}
