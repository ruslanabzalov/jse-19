package tsc.abzalov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;

import static tsc.abzalov.tm.enumeration.CommandType.SYSTEM_COMMAND;

public final class SystemVersionCommand extends AbstractCommand {

    @Override
    @NotNull
    public String getCommandName() {
        return "version";
    }

    @Override
    @NotNull
    public String getCommandArgument() {
        return "-v";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Shows application version.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return SYSTEM_COMMAND;
    }

    @Override
    public void execute() {
        System.out.println("Version: 1.0.0\n");
    }

}
