package tsc.abzalov.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IAuthService;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;
import tsc.abzalov.tm.exception.general.CommandInitException;

import static tsc.abzalov.tm.enumeration.CommandType.AUTH_COMMAND;
import static tsc.abzalov.tm.util.InputUtil.*;

public final class AuthRegisterCommand extends AbstractCommand {

    @Override
    @NotNull
    public String getCommandName() {
        return "register";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Register new user.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return AUTH_COMMAND;
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null) throw new CommandInitException();
        @NotNull final IAuthService authService = serviceLocator.getAuthService();
        System.out.println("CREATE USER\n");
        @NotNull final String login = inputLogin();
        @NotNull final String password = inputPassword();
        @NotNull final String firstName = inputFirstName();
        @Nullable final String lastName = inputLastName();
        @NotNull final String email = inputEmail();
        authService.register(login, password, firstName, lastName, email);
        System.out.println("User was successfully registered.");
    }

}
