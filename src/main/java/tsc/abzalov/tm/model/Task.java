package tsc.abzalov.tm.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import static tsc.abzalov.tm.constant.LiteralConst.DEFAULT_REFERENCE;

public final class Task extends BusinessEntity {

    @Nullable
    private String projectId;

    public Task(@NotNull final String name, @NotNull final String description, @NotNull final String userId) {
        super(name, description, userId);
    }

    @Nullable
    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(@Nullable final String projectId) {
        this.projectId = projectId;
    }

    @Override
    @NotNull
    public String toString() {
        final String correctProjectId = (this.projectId == null) ? DEFAULT_REFERENCE : this.projectId;
        return super.toString().replace("]", "; Project ID: " + correctProjectId + "]");
    }

}
